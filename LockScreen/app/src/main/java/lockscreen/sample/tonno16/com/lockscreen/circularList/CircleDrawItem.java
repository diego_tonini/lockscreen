package lockscreen.sample.tonno16.com.lockscreen.circularList;

import android.graphics.Bitmap;

/**
 * Created by Diego on 13/08/2014.
 */
public class CircleDrawItem {
    public Bitmap mIconBitmap;
    public double mAngle;
}
