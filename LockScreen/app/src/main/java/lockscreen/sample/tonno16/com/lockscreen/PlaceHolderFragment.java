package lockscreen.sample.tonno16.com.lockscreen;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.anupcowkur.wheelmenu.WheelMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class PlaceHolderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private Context context;

    private CustomTextView customTime;
    private WheelMenu wheelMenu;
    private ListView listViewImage;

    public PlaceHolderFragment() {
        // Required empty public constructor
    }

    public static PlaceHolderFragment newInstance(int sectionNumber) {
        PlaceHolderFragment fragment = new PlaceHolderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_place_holder, container, false);

            customTime = (CustomTextView) rootView.findViewById(R.id.customTime);
            resetTime();
            listViewImage = (ListView) rootView.findViewById(R.id.listViewImage);

        List<Integer> list = new ArrayList<Integer>();
        list.add(R.drawable.facebook);
        list.add(R.drawable.youtube);
        list.add(R.drawable.twitter);
        list.add(R.drawable.facebook);
        ImageAdapter imageAdapter = new ImageAdapter(getActivity().getApplicationContext(),R.layout.row_imageview,list);
        listViewImage.setAdapter(imageAdapter);

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        resetTime();
    }

    /* twe reset textview time correctly */
    private void resetTime(){
        Calendar calendar = Calendar.getInstance();
        customTime.setText(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))+" : "+String.valueOf(calendar.get(Calendar.MINUTE)));
    }

}
