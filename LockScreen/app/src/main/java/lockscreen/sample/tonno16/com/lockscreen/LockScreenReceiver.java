package lockscreen.sample.tonno16.com.lockscreen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Diego on 11/08/2014.
 */
public class LockScreenReceiver extends BroadcastReceiver {

    private static final String TAG = "service_lock_screen";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG,"onReceive");

        Intent i = new Intent();
        i.setClassName("lockscreen.sample.tonno16.com.lockscreen","lockscreen.sample.tonno16.com.lockscreen.LockScreenActivity");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }
}
