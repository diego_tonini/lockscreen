package lockscreen.sample.tonno16.com.lockscreen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Diego on 15/08/2014.
 */
public class ImageAdapter extends ArrayAdapter<Integer> {

    private List<Integer> listResId;
    private Context context;
    private LayoutInflater inflater;
    private int resource;

    public ImageAdapter(Context context, int imageViewResId ,List<Integer> listResId) {
        super(context, imageViewResId, listResId );
        this.listResId = listResId;
        this.context = context;
        resource = imageViewResId;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        // Recuperiamo l'oggetti che dobbiamo inserire a questa posizione
        int idImage  = getItem(position);
        ViewHolder holder;

        if (v == null) {

            v = inflater.inflate(resource, parent, false);

            holder = new ViewHolder();
            holder.imageView = (ImageView) v.findViewById(R.id.imgRowItem);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();

        }

        CalendarView c;


        float from = 34;
        float to = 360;
        holder.imageView.setImageResource(idImage);
        holder.imageView.setAnimation(new RotateAnimation(from,to));
        return v;
    }

    private static class ViewHolder {
        ImageView imageView;
    }

}
