package lockscreen.sample.tonno16.com.lockscreen;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class LockScreenService extends Service {

    private static String TAG = "service_lock_screen";

    private PowerManager pm;
    private KeyguardManager km;
    private WakeLock w1;
    private KeyguardLock k1;

    private LockScreenReceiver lockScreenReceiver = new LockScreenReceiver();

    @Override
    public void onCreate(){
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        registerReceiver(lockScreenReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        setLockScreen(false);
        Log.i(TAG,"onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy(){
        unregisterReceiver(lockScreenReceiver);
        setLockScreen(true);
        Log.i(TAG,"onDestroy");
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
       return null;
    }

    private void setLockScreen(boolean defaultOn){
        pm = (PowerManager) getSystemService(POWER_SERVICE);
        w1 = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "INFO");
        w1.acquire();

        km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        k1 = km.newKeyguardLock("name");

        if(!defaultOn) {
            k1.disableKeyguard();
        } else {
            k1.reenableKeyguard();
        }


    }
}
